# Shortly

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Flutter Front-end Development

### Flutter Version
This app currently is version-locked to Flutter 2.2.1  This version of flutter can be obtained by doing a "git checkout 2.2.1" inside your flutter install, it it comes from git.

If you are using a non-git install, the version can be found [here](https://flutter.dev/docs/development/tools/sdk/releases?tab=macos)

### PR Checklist

* Run `flutter pub get` and commit the resulting `pubspec.lock` (if changes were made to `pubspec.yaml`)
* Run `flutter format` *only* on the files you have changed
* Include updated supporting documentation and/or README
* Include updated tests as needed

### Adding/Changing Dependencies (in `pubspec.yaml`)

Checklist for adding or changing dependencies:
* Do you *really* need it?
* Does it compare favorably over other suitable packages?
* Is the license compatible with commercial development and deployment?
* Is it mature enough for production?
* Is it actively being developed and supported?
* Does it come from a reasonably stable and trusted source?

If changes are made to `pubspec.yaml`, run `flutter pub get` and include the resulting `pubspec.lock` file with the `pubspec.yaml` file in your PR.

## [SHRTCODE](https://shrtco.de/docs/)

This API is used to shorten the URLs

## Libraries Used

### [http](https://pub.dev/packages/http)

* http package is used to call the RESTFUL APIs to get the functionality done

### [flutter_bloc](https://pub.dev/packages/flutter_bloc)

* Bloc Pattern is used to handle the state of the application

### [floor](https://pub.dev/packages/floor)

* floor package is used to handle the local data storage. This package is buit on the top of sqflite.

### [flutter_svg](https://pub.dev/packages/http)

* To display .svg images, we have used this plugin in our project.



class Benefit {

  String icon;
  String title;
  String description;

  Benefit({
    this.icon = '',
    this.title = '',
    this.description = ''
  });

}
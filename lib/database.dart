import 'dart:async';
import 'package:floor/floor.dart';
import 'package:shortly/db_utils/dao/converted_item_dao.dart';
import 'package:shortly/db_utils/entity/converted_entity.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [ConvertedEntity])
abstract class AppDatabase extends FloorDatabase {
  ConvertedItemsDao get itemDao;
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shortly/routes/routes.dart';
import 'package:shortly/views/benefits_screen.dart';
import 'package:shortly/views/main_screen.dart';
import 'package:shortly/views/start_screen.dart';

class RouteGenerator {
  static Route<dynamic> mainRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.BENEFITS:
        return CupertinoPageRoute(
            builder: (ctx) => BenefitsScreen());
      case Routes.MAIN:
        return CupertinoPageRoute(
            builder: (ctx) => MainScreen());
      default:
        return CupertinoPageRoute(
            builder: (ctx) => StartScreen());
    }
  }
}
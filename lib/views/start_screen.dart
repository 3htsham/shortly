import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shortly/config/app_assets.dart';
import 'package:shortly/config/constants.dart';
import 'package:shortly/routes/routes.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: (size.height/100) * 8.8),
              constraints: BoxConstraints(
                  minHeight: constraints.maxHeight
              ),
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [

                    SvgPicture.asset(AppAssets.logo),

                    SvgPicture.asset(AppAssets.illustration,
                      width: size.width,
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0),
                      child: Text(S.moreThanJustShorterLinks, style: textTheme.headline3, textAlign: TextAlign.center,),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0),
                      child: Text(S.buildYourBrandRecognition, style: textTheme.bodyText1, textAlign: TextAlign.center,),
                    ),

                    SizedBox(height: 20,),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0),
                      child: ElevatedButton(
                          onPressed: (){
                            Navigator.of(context).pushReplacementNamed(Routes.BENEFITS);
                          },
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(S.start.toUpperCase(), style: textTheme.subtitle1?.copyWith(color: theme.scaffoldBackgroundColor),),
                            ),
                          )),
                    ),

                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

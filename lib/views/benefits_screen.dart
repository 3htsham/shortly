import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shortly/config/app_assets.dart';
import 'package:shortly/config/app_color.dart';
import 'package:shortly/config/constants.dart';
import 'package:shortly/elements/benefit_item.dart';
import 'package:shortly/models/benefit.dart';
import 'package:shortly/routes/routes.dart';

class BenefitsScreen extends StatefulWidget {
  const BenefitsScreen({Key? key}) : super(key: key);

  @override
  _BenefitsScreenState createState() => _BenefitsScreenState();
}

class _BenefitsScreenState extends State<BenefitsScreen> {

  int currentIndex = 0;

  List<Benefit> benefits = [
    Benefit(
      icon: AppAssets.diagramIcon,
      title: S.brandRecognition,
      description: S.boostYourBrandRecognition
    ),
    Benefit(
        icon: AppAssets.gaugeIcon,
        title: S.detailedRecords,
        description: S.gainInsightsInToWho
    ),
    Benefit(
        icon: AppAssets.toolsIcon,
        title: S.fullyCustomizable,
        description: S.improveBrandAwareness
    ),
  ];


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: AppColors.extraLightColor(1),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: (size.height/100) * 8.8),
              constraints: BoxConstraints(
                  minHeight: constraints.maxHeight
              ),
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [

                    SvgPicture.asset(AppAssets.logo),

                    Spacer(),

                    Spacer(),

                    Container(
                      height: (size.height / 100) * 41.8,
                      child: PageView.builder(
                        scrollDirection: Axis.horizontal,
                        onPageChanged: (idx) {
                          setState(() {
                            this.currentIndex = idx;
                          });
                        },
                        itemCount: this.benefits.length,
                        itemBuilder: (context, index) {
                          final _bnft = this.benefits[index];
                          final double avatarRadius = 40;


                          return BenefitItem(
                            benefit: _bnft,
                            avatarRadius: avatarRadius,
                          );
                        },
                      ),
                    ),

                    Container(
                      height: 14,
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(this.benefits.length, (index) {
                          return AnimatedContainer(
                            // padding: EdgeInsets.all(1),
                            margin: EdgeInsets.all(2),
                            width: 10,
                            duration: Duration(microseconds: 300),
                            decoration: BoxDecoration(
                                color: this.currentIndex == index ? AppColors.neutralGray(1) : Colors.transparent,
                                border: Border.all(width: 1, color: AppColors.neutralGray(1)),
                                borderRadius: BorderRadius.circular(100)
                            ),
                          );
                        }),
                      ),
                    ),

                    Spacer(),

                    TextButton(onPressed: (){
                      Navigator.of(context).pushReplacementNamed(Routes.MAIN);
                    }, child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(S.skip, style: textTheme.subtitle2,),
                      ),
                    )),

                    Spacer(),

                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}




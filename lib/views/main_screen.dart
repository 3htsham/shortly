import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shortly/config/app_assets.dart';
import 'package:shortly/config/app_color.dart';
import 'package:shortly/config/constants.dart';
import 'package:shortly/api/repository.dart' as repo;
import 'package:shortly/elements/shortened_link_item.dart';
import 'package:shortly/models/converted.dart';
import 'package:shortly/state/link_cubit.dart';
import 'package:shortly/state/links_state.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _textEditingController = TextEditingController();
  bool isError = false;
  String link = '';

  bool isLoading = false;

  List<Converted> shortened = [];

  void validateForm() async {
    if (!this.isLoading) {
      if (this._formKey.currentState != null) {
        if (this._formKey.currentState!.validate()) {
          this._formKey.currentState!.save();
          setState(() {
            this.isLoading = true;
          });
          final response = await shortenLink(this.link);
          if (response is String) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(response),
            ));
          } else if (response is Converted) {
            //TODO: Add to SQLite
            setState(() {
              this.shortened.add(response);
            });
          } else {
            //TODO: Didn't converted
          }
        } else {
          _textEditingController.text = S.pleaseAddALinkHere;
          setState(() {
            this.isError = true;
          });
        }
      }
    }
  }

  Future<dynamic> shortenLink(String urlToConvert) async {
    return repo.getInterestsListFuture(urlToConvert).then((value) {
      if (value != null) {
        return value;
      } else {
        return false;
      }
    }, onError: (e) {
      return 'Error shortening url';
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    LinksState().closeStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return BlocProvider(
      create: (_) => LinkCubit(),
      child: Scaffold(
        backgroundColor: AppColors.extraLightColor(1),
        body: Container(
          height: size.height,
          child: BlocBuilder<LinkCubit, LinksState>(
            builder: (context, state) {


              final inpBorder = OutlineInputBorder(
                  borderRadius: BorderRadius.circular(3),
                  borderSide: BorderSide(width: 1, color: theme.scaffoldBackgroundColor));
              final errorInpBorder = OutlineInputBorder(
                  borderRadius: BorderRadius.circular(3),
                  borderSide: BorderSide(width: 1, color: AppColors.secondaryColor(1)));
              var fieldTextStyle = textTheme.bodyText1;
              var errorFieldStyle = fieldTextStyle?.copyWith(color: AppColors.secondaryColor(1));

              return StreamBuilder(
                // initialData: List,
                stream: state.shortenedItemsController.stream,
                builder: (context, AsyncSnapshot<List<Converted>> snap) {
                  bool listExists = snap.hasData && snap.data != null && snap.data!.isNotEmpty;

                  return IntrinsicHeight(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          height: !listExists ? 50 : 80,
                        ),
                        !listExists
                            ? SvgPicture.asset(AppAssets.logo)
                            : Text(
                          S.yourLinkHistory,
                          style: textTheme.bodyText1,
                        ),
                        listExists
                            ? Expanded(
                          flex: 10,
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 22, vertical: 10),
                            child: SingleChildScrollView(
                              child: Column(
                                children: List.generate(snap.data!.length,
                                        (index) {
                                  final itm = snap.data![index];
                                      return ShortenedLinkItem(
                                        item: itm,
                                        iCopied: state.copiedLink == itm.shortLink,
                                        onCopy: (){
                                          //TODO: Implement Item Copy
                                          context.read<LinkCubit>().copyLink(itm.shortLink ?? '');
                                        },
                                        onDelete: (){
                                          context.read<LinkCubit>().deleteItem(itm);
                                        },
                                      );
                                    }),
                              ),
                            ),
                          ),
                        )
                            : Expanded(
                          flex: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SvgPicture.asset(
                                AppAssets.illustration,
                                width: size.width,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 40.0),
                                child: Column(
                                  children: [
                                    Text(
                                      S.letsGetStarted,
                                      style: textTheme.headline4,
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      S.pasteYourFirstLink,
                                      style: textTheme.bodyText1,
                                      textAlign: TextAlign.center,
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),

                        Expanded(
                          flex: 3,
                          child: Container(
                            width: size.width,
                            decoration: BoxDecoration(
                              color: theme.primaryColorDark,
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                    top: 0,
                                    right: 0,
                                    child: SvgPicture.asset(AppAssets.shape)),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 40.0, vertical: 40.0),
                                  child: Form(
                                    key: state.formKey,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Stack(
                                          children: [
                                            TextFormField(
                                              validator: (value) =>
                                              value == null || value.length < 2
                                                  ? ''
                                                  : null,
                                              controller: state.textEditingController,
                                              style: state.isError ? errorFieldStyle : fieldTextStyle,
                                              maxLines: 1,
                                              keyboardType: TextInputType.url,
                                              textAlign: state.isError
                                                  ? TextAlign.center
                                                  : TextAlign.start,
                                              decoration: InputDecoration(
                                                  contentPadding: EdgeInsets.only(
                                                      right: 10, left: 10),
                                                  filled: true,
                                                  fillColor:
                                                  theme.scaffoldBackgroundColor,
                                                  border: inpBorder,
                                                  enabledBorder: inpBorder,
                                                  focusedBorder: inpBorder,
                                                  errorBorder: errorInpBorder,
                                                  disabledBorder: inpBorder,
                                                  focusedErrorBorder: errorInpBorder,
                                                  errorStyle: TextStyle(
                                                      height: 0, fontSize: 1)),
                                              onSaved: (value) {
                                                state.link = value ?? '';
                                              },
                                              onChanged: (value) {
                                                if (state.isError) {
                                                  context.read<LinkCubit>().onTextChanged();
                                                }
                                              },
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        context.read<LinkCubit>().state.isLoading
                                            ? SizedBox(
                                          height: 40,
                                          child: CircularProgressIndicator(),
                                        )
                                            : ElevatedButton(
                                            onPressed: () {
                                              //TODO: Shorten URL Here
                                              context.read<LinkCubit>().validateForm(context);
                                            },
                                            child: Center(
                                              child: Padding(
                                                padding:
                                                const EdgeInsets.all(10.0),
                                                child: Text(
                                                  S.shortenIt.toUpperCase(),
                                                  style: textTheme.subtitle1
                                                      ?.copyWith(
                                                      color: theme
                                                          .scaffoldBackgroundColor),
                                                ),
                                              ),
                                            ))
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );

                },
              );

            },
          ),
        ),
      ),
    );

    /*
    return Scaffold(
      backgroundColor: AppColors.extraLightColor(1),
      body: Container(
        height: size.height,
        child: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: this.shortened.isEmpty ? 30 : 80,
              ),
              this.shortened.isEmpty
                  ? SvgPicture.asset(AppAssets.logo)
                  : Text(
                      S.yourLinkHistory,
                      style: textTheme.bodyText1,
                    ),

              /*
              Expanded(
                flex: 10,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 22, vertical: 10),
                  child: SingleChildScrollView(
                    child: Column(
                      children: List.generate(
                          this.shortened.length,
                              (index)  {
                                final shrt = this.shortened[index];


                            return Container(
                            margin: EdgeInsets.symmetric(vertical: 8,),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 22,),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                        child: Text(
                                          shrt.shortenedLink, style: textTheme.bodyText1,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      IconButton(
                                        onPressed: (){},
                                        icon: Icon(Icons.delete, color: Colors.black,),
                                      ),
                                    ],
                                  ),
                                ),

                                Divider(
                                  height: 1,
                                  color: Colors.grey,
                                ),

                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 22, vertical: 14),
                                  child: Text(
                                    shrt.link,
                                    style: textTheme.bodyText1?.copyWith(color: theme.primaryColor),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),

                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 22,),
                                  child: ElevatedButton(
                                      onPressed: () {
                                        //TODO: Shorten URL Here
                                      },
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(
                                            S.copy.toUpperCase(),
                                            style: textTheme.subtitle1?.copyWith(
                                                color: theme
                                                    .scaffoldBackgroundColor),
                                          ),
                                        ),
                                      )),
                                ),

                                SizedBox(height: 20,),

                              ],
                            ),
                          );
                        }
                      ),
                    ),
                  ),
                ),
              ),
              */

              Expanded(
                flex: 10,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset(
                      AppAssets.illustration,
                      width: size.width,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0),
                      child: Column(
                        children: [
                          Text(
                            S.letsGetStarted,
                            style: textTheme.headline4,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            S.pasteYourFirstLink,
                            style: textTheme.bodyText1,
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  width: size.width,
                  decoration: BoxDecoration(
                    color: theme.primaryColorDark,
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                          top: 0,
                          right: 0,
                          child: SvgPicture.asset(AppAssets.shape)),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 40.0, vertical: 40.0),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Stack(
                                children: [
                                  TextFormField(
                                    validator: (value) =>
                                        value == null || value.length < 2
                                            ? ''
                                            : null,
                                    controller: _textEditingController,
                                    style: fieldTextStyle,
                                    maxLines: 1,
                                    keyboardType: TextInputType.url,
                                    textAlign: this.isError
                                        ? TextAlign.center
                                        : TextAlign.start,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                            right: 10, left: 10),
                                        filled: true,
                                        fillColor:
                                            theme.scaffoldBackgroundColor,
                                        border: inpBorder,
                                        enabledBorder: inpBorder,
                                        focusedBorder: inpBorder,
                                        errorBorder: errorInpBorder,
                                        disabledBorder: inpBorder,
                                        focusedErrorBorder: errorInpBorder,
                                        errorStyle:
                                            TextStyle(height: 0, fontSize: 1)),
                                    onSaved: (value) {
                                      this.link = value ?? '';
                                    },
                                    onChanged: (value) {
                                      if (this.isError) {
                                        setState(() {
                                          this.isError = false;
                                        });
                                        this._textEditingController.text = '';
                                      }
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              this.isLoading
                                  ? SizedBox(
                                      height: 40,
                                      child: CircularProgressIndicator(),
                                    )
                                  : ElevatedButton(
                                      onPressed: () {
                                        //TODO: Shorten URL Here
                                        this.validateForm();
                                      },
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(
                                            S.shortenIt.toUpperCase(),
                                            style: textTheme.subtitle1?.copyWith(
                                                color: theme
                                                    .scaffoldBackgroundColor),
                                          ),
                                        ),
                                      ))
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
    */
  }
}

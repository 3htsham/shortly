import 'package:flutter/material.dart';
import 'package:shortly/config/app_color.dart';
import 'package:shortly/config/app_style.dart';
import 'package:shortly/config/constants.dart';
import 'package:shortly/routes/route_generator.dart';
import 'package:shortly/routes/routes.dart';
import 'package:shortly/settings.dart' as settings;

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    settings.initSettings();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: S.app_name,
      theme: ThemeData(
        primaryColor: AppColors.primaryColor(1),
        primaryColorDark: AppColors.primaryColorDark(1),
        scaffoldBackgroundColor: AppColors.lightColor(1),
        backgroundColor: AppColors.lightColor(1),
        accentColor: AppColors.secondaryColor(1),
        fontFamily: S.fontFamily,
        textTheme: AppStyle.getAppTextTheme(context),
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
                primary: AppColors.primaryColor(1),
              textStyle: TextStyle(color: AppColors.lightColor(1)),
              elevation: 0,
            )),
      ),
      onGenerateRoute: RouteGenerator.mainRoute,
      initialRoute: Routes.ROOT,
    );
  }
}

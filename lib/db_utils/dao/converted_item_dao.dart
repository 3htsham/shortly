import 'package:floor/floor.dart';
import 'package:shortly/db_utils/entity/converted_entity.dart';


@dao
abstract class ConvertedItemsDao {
  @Query('SELECT * FROM ConvertedEntity')
  Future<List<ConvertedEntity>> findAllItemsList();

  @insert
  Future<void> insertItem(ConvertedEntity item);

  @insert
  Future<void> insertItems(List<ConvertedEntity> items);

  @Query('DELETE * FROM ConvertedEntity')
  Future<List<ConvertedEntity>> deleteAllItems();

  @Query('DELETE FROM ConvertedEntity WHERE id = :id')
  Future<List<ConvertedEntity>> deleteItem(int id);

  @Query('DELETE FROM ConvertedEntity WHERE code = :code')
  Future<List<ConvertedEntity>> deleteItemByCode(String code);

}
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shortly/config/constants.dart';
import 'package:shortly/db_utils/entity/converted_entity.dart';
import 'package:shortly/models/converted.dart';
import 'package:shortly/state/links_state.dart';
import 'package:shortly/api/repository.dart' as repo;
import 'package:shortly/settings.dart' as settings;

class LinkCubit extends Cubit<LinksState> {
  LinkCubit() : super(LinksState()) {
    getLinks();
  }

  void onTextChanged(){
    emit(state.copyWith(isError: false));
    state.textEditingController.text =
    '';
  }

  void copyLink(String link) {
    emit(state.copyWith(copiedLink: link));
    Clipboard.setData(ClipboardData(text: "your text"));
  }

  void validateForm(BuildContext context) async {
    if (!state.isLoading) {
      if (state.formKey.currentState != null) {
        if (state.formKey.currentState!.validate()) {
          state.formKey.currentState!.save();
          emit(state.copyWith(isLoading: true));
          final response = await shortenLink(state.link);
          emit(state.copyWith(isLoading: false));
          if (response is String) {
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text(response),)
            );
          } else if (response is Converted) {
            //TODO: Add to SQLite
            if (state.shortened == null) {
              state.shortened = [];
            }
            insertItem(response);
            final shortened = state.shortened;
            shortened!.add(response);
            emit(state.copyWith(shortened: shortened));
            state.shortenedItemsController.add(state.shortened!);
          } else {
            //TODO: Didn't converted
          }
        } else {
          emit(state.copyWith(isError: true));
          state.textEditingController.text = S.pleaseAddALinkHere;
        }
      }
    }
  }

  void getLinks() async {
    if (settings.database != null) {
      var items = await settings.database!.itemDao.findAllItemsList();
      if (state.shortened == null) {
        state.shortened = [];
      }
      state.shortened!.clear();
      emit(state.copyWith(shortened: state.shortened));
      items.forEach((element) {
        state.shortened!.add(Converted.fromJson(element.toJson()));
      });
      state.shortenedItemsController.sink.add(state.shortened!);
    }
  }

  insertItem(Converted converted) async {
    if (settings.database != null) {
      final item = await settings.database!.itemDao.insertItem(ConvertedEntity.fromJson(converted.toJson()));
    }
  }

  deleteItem(Converted converted) async {
    if (settings.database != null) {
      if(converted.id != null) {
        await settings.database!.itemDao.deleteItem(converted.id ?? 0);
      } else {
        await settings.database!.itemDao.deleteItemByCode(converted.code ?? '');
      }
      getLinks();
    }
  }

  Future<dynamic> shortenLink(String urlToConvert) async {
    return repo.getInterestsListFuture(urlToConvert).then((value) {
      if (value != null) {
        return value;
      } else {
        return false;
      }
    }, onError: (e) {
      return 'Error shortening url';
    });
  }
}

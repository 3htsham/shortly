import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shortly/models/converted.dart';

class LinksState extends Equatable {
  String link = '';

  bool isLoading = false;

  List<Converted>? shortened = [];

  GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController textEditingController = TextEditingController();
  bool isError = false;
  String copiedLink = '';

  StreamController<List<Converted>> shortenedItemsController = StreamController<List<Converted>>();

  LinksState({
    this.isLoading = false,
    this.link = '',
    this.shortened,
    this.isError = false,
    this.copiedLink = '',
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
    link, isLoading, shortened, isError, copiedLink,
  ];

  LinksState copyWith({String? link, bool? isLoading, List<Converted>? shortened, bool? isError, String? copiedLink}) {
    return LinksState(
      link: link ?? this.link,
      isLoading: isLoading ?? this.isLoading,
      shortened: shortened ?? this.shortened,
      isError: isError ?? this.isError,
        copiedLink: copiedLink ?? this.copiedLink
    );
  }

  closeStream(){
    shortenedItemsController.close();
  }

}
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  ConvertedItemsDao? _itemDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `ConvertedEntity` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `code` TEXT, `shortLink` TEXT, `fullShortLink` TEXT, `shortLink2` TEXT, `fullShortLink2` TEXT, `shareLink` TEXT, `fullShareLink` TEXT, `originalLink` TEXT)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  ConvertedItemsDao get itemDao {
    return _itemDaoInstance ??= _$ConvertedItemsDao(database, changeListener);
  }
}

class _$ConvertedItemsDao extends ConvertedItemsDao {
  _$ConvertedItemsDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _convertedEntityInsertionAdapter = InsertionAdapter(
            database,
            'ConvertedEntity',
            (ConvertedEntity item) => <String, Object?>{
                  'id': item.id,
                  'code': item.code,
                  'shortLink': item.shortLink,
                  'fullShortLink': item.fullShortLink,
                  'shortLink2': item.shortLink2,
                  'fullShortLink2': item.fullShortLink2,
                  'shareLink': item.shareLink,
                  'fullShareLink': item.fullShareLink,
                  'originalLink': item.originalLink
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<ConvertedEntity> _convertedEntityInsertionAdapter;

  @override
  Future<List<ConvertedEntity>> findAllItemsList() async {
    return _queryAdapter.queryList('SELECT * FROM ConvertedEntity',
        mapper: (Map<String, Object?> row) => ConvertedEntity(
            id: row['id'] as int?,
            originalLink: row['originalLink'] as String?,
            fullShortLink: row['fullShortLink'] as String?,
            code: row['code'] as String?,
            fullShareLink: row['fullShareLink'] as String?,
            fullShortLink2: row['fullShortLink2'] as String?,
            shareLink: row['shareLink'] as String?,
            shortLink: row['shortLink'] as String?,
            shortLink2: row['shortLink2'] as String?));
  }

  @override
  Future<List<ConvertedEntity>> deleteAllItems() async {
    return _queryAdapter.queryList('DELETE * FROM ConvertedEntity',
        mapper: (Map<String, Object?> row) => ConvertedEntity(
            id: row['id'] as int?,
            originalLink: row['originalLink'] as String?,
            fullShortLink: row['fullShortLink'] as String?,
            code: row['code'] as String?,
            fullShareLink: row['fullShareLink'] as String?,
            fullShortLink2: row['fullShortLink2'] as String?,
            shareLink: row['shareLink'] as String?,
            shortLink: row['shortLink'] as String?,
            shortLink2: row['shortLink2'] as String?));
  }

  @override
  Future<List<ConvertedEntity>> deleteItem(int id) async {
    return _queryAdapter.queryList('DELETE FROM ConvertedEntity WHERE id = ?1',
        mapper: (Map<String, Object?> row) => ConvertedEntity(
            id: row['id'] as int?,
            originalLink: row['originalLink'] as String?,
            fullShortLink: row['fullShortLink'] as String?,
            code: row['code'] as String?,
            fullShareLink: row['fullShareLink'] as String?,
            fullShortLink2: row['fullShortLink2'] as String?,
            shareLink: row['shareLink'] as String?,
            shortLink: row['shortLink'] as String?,
            shortLink2: row['shortLink2'] as String?),
        arguments: [id]);
  }

  @override
  Future<List<ConvertedEntity>> deleteItemByCode(String code) async {
    return _queryAdapter.queryList(
        'DELETE FROM ConvertedEntity WHERE code = ?1',
        mapper: (Map<String, Object?> row) => ConvertedEntity(
            id: row['id'] as int?,
            originalLink: row['originalLink'] as String?,
            fullShortLink: row['fullShortLink'] as String?,
            code: row['code'] as String?,
            fullShareLink: row['fullShareLink'] as String?,
            fullShortLink2: row['fullShortLink2'] as String?,
            shareLink: row['shareLink'] as String?,
            shortLink: row['shortLink'] as String?,
            shortLink2: row['shortLink2'] as String?),
        arguments: [code]);
  }

  @override
  Future<void> insertItem(ConvertedEntity item) async {
    await _convertedEntityInsertionAdapter.insert(
        item, OnConflictStrategy.abort);
  }

  @override
  Future<void> insertItems(List<ConvertedEntity> items) async {
    await _convertedEntityInsertionAdapter.insertList(
        items, OnConflictStrategy.abort);
  }
}

import 'dart:io';
import 'package:floor/floor.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:shortly/config/constants.dart';
import 'package:sqflite/sqflite.dart';

import '../../database.dart';

AppDatabase? database;

Future<void> initSettings() async {
  await _openOrCreateSQFDatabase();
  database = await $FloorAppDatabase.databaseBuilder(S.dbName).build();
}

Future<void> _openOrCreateSQFDatabase() async {
  String dbPath = "";
  if(!kIsWeb) {
    dbPath = await getDatabasesPath();
  } else {
    var p = inMemoryDatabasePath;
    dbPath = p;
  }
  ///DB
  var path = join(dbPath, S.dbName);
  var exists = await databaseExists(path);
  if(!exists) {
    try {
      database = await $FloorAppDatabase.databaseBuilder(path).build();
    } catch (e) {
      print(e);
    }
  }
}
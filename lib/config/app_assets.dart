class AppAssets {

  static const String diagramIcon = 'assets/icons/diagram.svg';
  static const String gaugeIcon = 'assets/icons/gauge.svg';
  static const String logo = 'assets/icons/logo.svg';
  static const String toolsIcon = 'assets/icons/tools.svg';
  static const String illustration = 'assets/images/illustration.svg';
  static const String shape = 'assets/images/shape.svg';

}
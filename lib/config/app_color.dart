import 'package:flutter/material.dart';

class AppColors {

  static Color cyan = Color(0xFF2ACFCF);
  static Color darkViolet = Color(0xFF3B3054);

  static Color red = Color(0xFFF46262);

  static Color lightGray = Color(0xFFBFBFBF);
  static Color gray = Color(0xFF9E9AA7);
  static Color grayishViolet = Color(0xFF35323E);
  static Color veryDarkViolet = Color(0xFF232127);

  static Color white = Color(0xFFFFFFFF);
  static Color offWhite = Color(0xFFF0F1F6);

  static Color primaryColor(double opacity) => cyan.withOpacity(opacity);
  static Color primaryColorDark(double opacity) => darkViolet.withOpacity(opacity);

  static Color secondaryColor(double opacity) => red.withOpacity(opacity);

  static Color neutralGray(double opacity) => gray.withOpacity(opacity);
  static Color neutralGrayLight(double opacity) => lightGray.withOpacity(opacity);
  static Color neutralGrayViolet(double opacity) => grayishViolet.withOpacity(opacity);
  static Color neutralDarkViolet(double opacity) => veryDarkViolet.withOpacity(opacity);

  static Color lightColor(double opacity) => white.withOpacity(opacity);
  static Color extraLightColor(double opacity) => offWhite.withOpacity(opacity);

}
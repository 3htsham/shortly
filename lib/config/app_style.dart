import 'package:flutter/material.dart';
import 'package:shortly/config/app_color.dart';
import 'package:shortly/config/constants.dart';

class AppStyle {

  static TextTheme getAppTextTheme(BuildContext context) => TextTheme(
    headline1: mainAppTextStyle().copyWith(
      fontSize: 80.0,
    ),
    headline2: mainAppTextStyle().copyWith(
      fontSize: 48.0,
    ),
    headline3: mainAppTextStyle()
        .copyWith(fontSize: 34.0, fontWeight: FontWeight.w800),
    headline4: mainAppTextStyle()
        .copyWith(fontSize: 20.0, fontWeight: FontWeight.w800),
    headline5: mainAppTextStyle()
        .copyWith(fontSize: 16.0, fontWeight: FontWeight.w800),
    headline6: mainAppTextStyle()
        .copyWith(fontSize: 19.0, fontWeight: FontWeight.w800),
    subtitle1: mainAppTextStyle()
        .copyWith(fontSize: 20.0, fontWeight: FontWeight.w800),
    subtitle2: mainAppTextStyle()
        .copyWith(fontSize: 16.0, fontWeight: FontWeight.w600),
    bodyText1: mainAppTextStyle()
        .copyWith(fontSize: 17.0, fontWeight: FontWeight.w500),
    bodyText2: mainAppTextStyle()
        .copyWith(fontSize: 14.0, fontWeight: FontWeight.w400),
    caption: mainAppTextStyle()
        .copyWith(fontSize: 12.0, fontWeight: FontWeight.w400),
  );

  static TextStyle mainAppTextStyle() => TextStyle(
      fontFamily: S.fontFamily,
      fontWeight: FontWeight.w500,
      color: AppColors.neutralDarkViolet(1));

}
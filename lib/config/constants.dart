class S {
  static const dbName = 'shortly.db';
  static const fontFamily = 'Poppins';

  static const app_name = 'Shortly';

  static const moreThanJustShorterLinks = 'More than just shorter links';
  static const buildYourBrandRecognition = 'Build your brand\'s recognition and get detailed insights on how your links are performing.';
  static const start = 'Start';

  static const skip = 'Skip';
  static const brandRecognition = 'Brand Recognition';
  static const boostYourBrandRecognition = 'Boost your brand recognition with each click. Generic links don\'t mean a thing. Branded links help instil confidence in your content.';
  static const detailedRecords = 'Detailed Record';
  static const gainInsightsInToWho = 'Gain insights into who is clicking your links. Knowing when and where people engage with your content helps inform better decisions.';
  static const fullyCustomizable = 'Fully Customizable';
  static const improveBrandAwareness = 'Improve brand awareness and content discoverability through customizable links, supercharging audience engagement.';

  static const letsGetStarted = 'Let\'s get started!';
  static const pasteYourFirstLink = 'Paste your first link into\nthe field to shorten it';
  static const shortenALinkHere = 'Shorten a link here...';
  static const shortenIt = 'Shorten It!';
  static const pleaseAddALinkHere = 'Please add a link here';
  static const yourLinkHistory = 'Your Link History';
  static const copy = 'Copy';
  static const copied = 'Copied!';

}
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shortly/models/benefit.dart';

class BenefitItem extends StatelessWidget {
  Benefit? benefit;
  double avatarRadius;

  BenefitItem({Key? key, this.benefit, this.avatarRadius = 40}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24),
      child: Stack(
        children: [

          Container(
            margin: EdgeInsets.only(top: avatarRadius),
            padding: EdgeInsets.symmetric(horizontal: 40),
            decoration: BoxDecoration(
              color: theme.scaffoldBackgroundColor,

            ),
            child: Column(
              children: [
                Spacer(),
                // SizedBox(height: avatarRadius,),
                Text(benefit?.title ?? '', style: textTheme.subtitle1,),
                SizedBox(height: 20,),
                Text(benefit?.description ?? '', style: textTheme.bodyText1, textAlign: TextAlign.center,),
                Spacer(),
              ],
            ),
          ),

          Align(
            alignment: Alignment.topCenter,
            child: CircleAvatar(
              radius: avatarRadius,
              backgroundColor: theme.primaryColorDark,
              child: benefit?.icon != null ? SvgPicture.asset(
                  benefit?.icon ?? ''
              ) : SizedBox(),
            ),
          ),

        ],
      ),
    );
  }
}
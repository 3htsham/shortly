import 'package:flutter/material.dart';
import 'package:shortly/config/app_color.dart';
import 'package:shortly/config/constants.dart';
import 'package:shortly/models/converted.dart';

class ShortenedLinkItem extends StatelessWidget {
  final Converted? item;
  final VoidCallback? onDelete;
  final VoidCallback? onCopy;
  final bool iCopied;

  ShortenedLinkItem({Key? key,
    this.item,
    this.onCopy,
    this.onDelete,
    this.iCopied = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 8,),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 22,),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Text(
                    item?.fullShortLink ?? '',
                    style: textTheme.bodyText1,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                IconButton(
                  onPressed: (){
                    this.onDelete?.call();
                  },
                  icon: Icon(Icons.delete, color: Colors.black,),
                ),
              ],
            ),
          ),

          Divider(
            height: 1,
            color: Colors.grey,
          ),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: 22, vertical: 14),
            child: Text(
              item?.originalLink ?? '',
              style: textTheme.bodyText1?.copyWith(color: theme.primaryColor),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: 22,),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: iCopied ? AppColors.neutralDarkViolet(1) : AppColors.primaryColor(1)
              ),
                onPressed: () {
                  //TODO: Copy URL Here
                  if(!iCopied) {
                    this.onCopy?.call();
                  }
                },
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      this.iCopied ? S.copied.toUpperCase() : S.copy.toUpperCase(),
                      style: textTheme.subtitle1?.copyWith(
                          color: theme
                              .scaffoldBackgroundColor),
                    ),
                  ),
                )),
          ),

          SizedBox(height: 20,),

        ],
      ),
    );
  }
}

class Helper {
  static getResultData(Map<String, dynamic> data) {
    return data['result'] ?? {};
  }
}
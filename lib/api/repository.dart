import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shortly/helpers/helper.dart';
import 'package:shortly/models/converted.dart';

Future<Converted?> getInterestsListFuture(String urlToConvert) async {
  Converted? convertedResponse;
  //https://api.shrtco.de/v2/shorten?url=example.org/very/long/link.html
  final String url =
      'https://api.shrtco.de/v2/shorten?url=$urlToConvert';

  final client = new http.Client();

  final response = await client.get(
    Uri.parse(url),
  );

  if (response.body != null) {
    var body = response.body;
    if(json.decode(body)['ok'] == true) {
      final res = Helper.getResultData(json.decode(body));

      if (res != null) {
        convertedResponse = Converted.fromJson(res);
      }
    }
  }
  return convertedResponse;
}
